let mongoose = require('mongoose');

const EmployeeSchema = new mongoose.Schema({
    id : Number,
    employee_name: String,
    employee_salary: String,
    employee_age: String,
    created_date : {
        type: Date,
        default: Date.now()
    },
});

let Employee = mongoose.model("Employee", EmployeeSchema);

module.exports = Employee;

