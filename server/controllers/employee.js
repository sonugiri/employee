const Employee = require('../models/Employee');

//Simple version, without validation or sanitation
exports.create = function (req, res) {
    let employee = new Employee ({
        employee_name: req.body.employee_name,
        employee_salary: req.body.employee_salary,
        employee_age: req.body.employee_age,
    });

    employee.save((err) => {
        if (err)
        console.log(err);

        res.send('Product Created successfully')
    })
};

exports.detail = function (req, res) {
    Employee.findById(req.params.id, (err, employee) => {
        if (err)
        console.log(err);

        res.send(employee);
    })
};

exports.list = function (req, res) {
    Employee.find({}, (err, employees) => {
        if (err)
        console.log(err);

        res.send(employees);
    })
};

exports.employee_update = function (req, res) {
    Employee.findOneAndUpdate(req.params.id, {$set: req.body}, function (err, product) {
        if (err) return next(err);
        res.send(product);
    });
};

exports.employee_delete = function (req, res) {
    Employee.findOneAndDelete(req.params.id,  function (err) {
        if (err) return next(err);
        res.send('Deleted successfully');
    });
};