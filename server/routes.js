const express = require('express');

const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const employee_controller = require('./controllers/employee');


// a simple test url to check that all of our files are communicating correctly.

router.post('/create', employee_controller.create);

router.get('/detail/:id', employee_controller.detail);

router.get('/list', employee_controller.list);

router.put('/update/:id', employee_controller.employee_update);

router.delete('/delete/:id', employee_controller.employee_delete);

module.exports = router;