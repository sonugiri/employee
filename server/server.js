const express = require('express');
const mongoose = require('mongoose');

const app = express();

const bodyParser = require('body-parser');

const routes = require('./routes');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));
app.use('/employee', routes);

mongoose.connect('mongodb://employee:employee123@ds121135.mlab.com:21135/employee', { useNewUrlParser: true }, (err) => {
    if (err) console.log(`Error connecting`);
    console.log(`Db Connected`);
});

const Employee = require('./models/Employee');

const port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});