import React from 'react';
import { Link, NavLink  } from 'react-router-dom';

const Header = () => {
    return ( 
        <header>
      <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <Link className="navbar-brand" to="/">Employee List</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarCollapse">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="#"> <span className="sr-only"></span></a>
            </li>
            <li className="nav-item">
            <NavLink  to="/" className="nav-link">Home</NavLink >
            </li>
            <li className="nav-item">
              <NavLink  to="/add" className="nav-link">Create Employee</NavLink >
            </li>
            
          </ul>
        </div>
      </nav>
    </header>
     );
}
 
export default Header;