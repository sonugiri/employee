import React, { Component } from 'react';

class Search extends Component {
    state = {
        searchText : ''
    }

    onChangeHandle = (event) => {
        let search = event.target.value;
        this.setState({
            searchText : search
        });
        this.props.searchEmployee(search);
    }

    render() {
        return (
            <form>
                <div className="form-group">
                    <label forName="exampleInputEmail1">Search Employee:</label>
                    <input type="text"
                     className="form-control"
                      placeholder="Search"
                      value={this.state.searchText}
                      onChange= { this.onChangeHandle } />
                    
                </div>
            </form>
        );
    }
}

export default Search;