import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';

import Header from './Header';
import Employeelist from './Employeelist';
import Addemployee from './Addemployee';
import Search from './Search';
import Footer from './Footer';
import Loader from './Loader';

const ListWithLoading = Loader(Employeelist);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employees: [],
        }
        this.employeelist = '';

    }


    componentDidMount() {
        this.setState({
            isLoading: true
        })
        fetch('/employee/list')
            .then(res => res.json())
            .then(data => {
                this.employeelist = data;
                this.setState({
                    employees: data,
                    isLoading: false
                })
            });
    }

    searchEmployee(search) {
        let employ = this.employeelist;
        let searchList = employ.filter((emp) => {
            if (emp.employee_name.toLowerCase().includes(search.toLowerCase())) {
                return emp;
            }
        })

        this.setState({
            employees: searchList
        })
    }

    addEmployee = (data) => {
        let body = {
            employee_name: data.employee_name,
            employee_age: data.employee_age,
            employee_salary: data.employee_salary,
        }

        fetch('/employee/create', {
            header : {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            },
            method: 'POST',
            body : body,
        }, )
            .then(res => res)
            .then(data => {
                console.log(data);
            });
        
    }

    render() {
        return (
            <Router>
                <React.Fragment>
                    <Header />
                    <main role="main" className="container">
                        <Route exact path="/" render={() => {
                            return (
                                <React.Fragment>
                                    <Search searchEmployee={this.searchEmployee.bind(this)} />
                                    <ListWithLoading employeelist={this.state.employees}
                                        isLoading={this.state.isLoading} />
                                </React.Fragment>
                            )
                        }} />
                        <Route path="/add"
                        render={(props) => {
                            return (
                            <Addemployee addEmployee={this.addEmployee} { ...props } />
                            )
                        }}  />
                    </main>
                    <Footer />
                </React.Fragment>
            </Router>
        );
    }
}

export default App;
