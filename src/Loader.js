import React, { Component } from 'react';
import logo from './spinner.gif';

const Loader = (WrappedComponent) => () => {
        return{
            render() {
                if (!this.props.isLoading) {
                    return  <WrappedComponent {...this.props} />
                }
                return  <div className="col-12 text-center"><img src={logo} width="230" /></div>
                
            }
        }
        
    }

export default Loader;