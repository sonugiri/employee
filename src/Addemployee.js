import React, { Component } from 'react';

class Addemployee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            employee_name: '',
            employee_age: '',
            employee_salary: '',
            emp_err : {
                name_err : '',
                age_err : '',
                salary_err : '',
            }
        }
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onSubmitHandler = this.onSubmitHandler.bind(this);
    }

    onChangeHandler(event) {
        this.setState({
            [event.target.name] : event.target.value,
        });
    }

    onSubmitHandler(event) {
        event.preventDefault();
        var name_err = '';
        var err = false;
        if (!this.state.employee_name) {
            name_err = 'Name is required.'
            err = true;
        }
        if (!this.state.employee_age) {
            var age_err = 'Age is required.'
            err = true;
        }
        if (!this.state.employee_salary) {
            var salary_err = 'Salary is required.'
            err = true;
        }

        this.setState({
            emp_err : {
                name_err : name_err,
                age_err : age_err,
                salary_err : salary_err,
            }
        })
        if(err) return false;

        this.props.addEmployee(this.state);
    }

    render() {
        return (
            <form action="" onSubmit={ this.onSubmitHandler }>
                <div className="form-group">
                    <label htmlFor="name">Name:</label>
                    <input type="text" name="employee_name" className="form-control" value={this.state.employee_name}
                    onChange={this.onChangeHandler} />
                    { this.state.emp_err.name_err ? <span className="error">{ this.state.emp_err.name_err}</span> :
                    ''}
                </div>
                <div className="form-group">
                    <label htmlFor="age">Age:</label>
                    <input type="number" className="form-control" name="employee_age" value={this.state.employee_age}
                    onChange={this.onChangeHandler}/>
                     { this.state.emp_err.age_err ? <span className="error">{ this.state.emp_err.age_err}</span> :
                    ''}
                </div>
                <div className="form-group">
                    <label htmlFor="salary">Salary:</label>
                    <input type="text" className="form-control" name="employee_salary" value={this.state.employee_salary}
                    onChange={this.onChangeHandler}/>
                     { this.state.emp_err.salary_err ? <span className="error">{ this.state.emp_err.salary_err}</span> :
                    ''}
                </div>

                <button type="submit" className="btn btn-primary">Add</button>
            </form>
        );
    }
}

export default Addemployee;