import React, { Component } from 'react';

import Employee from './Employee';


class Employeelist extends Component {
    render() {
        let employeeList = this.props.employeelist;
        return (
            <table className="table">
                <thead className="thead-inverse">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Salary</th>
                        <th>Age</th>
                        <th>Image</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        employeeList.map((emp) => (
                            <Employee emp= {emp}/>
                        ))
                    }
                </tbody>
            </table>
        );
    }
}

export default Employeelist;