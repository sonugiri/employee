import React from 'react';

const Employee = (props) => {
    let emp = props.emp;
    return (
        <tr key={ emp.id }>
            <th scope="row">{ emp.id }</th>
            <td>{ emp.employee_name }</td>
            <td>{ emp.employee_salary }</td>
            <td>{ emp.employee_age }</td>
            <td></td>
        </tr>
    );
}

export default Employee;